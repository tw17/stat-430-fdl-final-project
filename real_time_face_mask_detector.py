import torch
import cv2

from PIL import Image

from cnn_model import transform, FaceMaskDetector

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def detect_faces(image, classifier):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    faces = classifier.detectMultiScale(
        gray,
    )
    return faces

if __name__ == "__main__":
    model = torch.load("Model/cnn_model.pt")
    model.eval()

    classifier = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

    cap = cv2.VideoCapture(0)
    cap.set(3,640) # set Width
    cap.set(4,480) # set Height

    while True:
        _, image = cap.read()
        
        faces = detect_faces(image, classifier)

        for x, y, w, h in faces:
            cv2.rectangle(image, (x, y), (x+w, y+h), (255, 0, 0), 2)

            roi_color = cv2.cvtColor(image[y+10:y+h-10, x+10:x+w-10], cv2.COLOR_BGR2RGB)

            pil_image = Image.fromarray(roi_color)

            tensor_image = transform(pil_image).unsqueeze(0).to(device)

            with torch.no_grad():
                label = model(tensor_image)
                print(label, torch.argmax(label, axis=1))

        cv2.imshow('video', image)

        k = cv2.waitKey(30) & 0xff
        if k == 27: # press 'ESC' to quit
            break
        
    cap.release()
    cv2.destroyAllWindows()