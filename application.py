import torch

import gradio as gr

from cnn_model import transform, CNNFaceMaskDetector

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

model = torch.load("Model/cnn_model.pt")
model.eval()

def predict(image):
    # Predict output
    output = model(transform(image).unsqueeze(0).to(device))
    
    # Extract label
    label = torch.argmax(output, axis=1)
    
    # Return correct class
    if label.item() == 0:
        return "Incorrect Mask"
    elif label.item() == 1:
        return "With Mask"
    else:
        return "Without Mask"

with gr.Blocks() as demo:
    image = gr.Image(label="Input", type="pil")
    prediction = gr.Textbox(label="Prediction")
    greet_btn = gr.Button("Predict")
    greet_btn.click(fn=predict, inputs=image, outputs=prediction)

demo.launch()